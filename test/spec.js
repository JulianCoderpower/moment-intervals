var expect = require('expect.js');
var endOf = require('../sources/endOf.js');
var isBetween = require('../sources/isBetween.js');
var isSame = require('../sources/isSame.js');

describe('Use endOf method of Moment.js', function(){
    it('should return a string', function(){
        var result = endOf();
        expect(result).to.be.a('string');
        expect(result).to.be.ok();
    });

    it('should return the last day of the week', function(){
        var result = endOf();
        expect(result).to.be.equal('7/3/2010');
    });

    it('should return a boolean', function(){
        var result = isBetween();
        expect(result).to.be(true);
    });

    it('should return a boolean', function(){
        var result = isSame();
        expect(result).to.be(true);
    });
});